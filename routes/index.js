var express = require('express');
var router = express.Router();
var Tesseract = require('tesseract.js');
const path = require('path');
var Jimp = require('jimp');



//multer object creation
var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/inputs/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
});

var upload = multer({ storage: storage });

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'OCR with Tesseract.js Example Project' });
});

/* POST image upload. */
router.post('/', upload.single('imageupload'),function(req, res) {

  let input_filename = req.file.filename;
  let output_filename = path.parse(input_filename).name + '.png';

  function callback(){
   Tesseract.recognize('public/outputs/' + output_filename, {
     lang: 'spa'
   })
     .then(function(result){
       console.log(result.text);
       res.send(result.text);
     });
  }
  Jimp.read('public/inputs/' + input_filename)
    .then(img => {
      return img
        .greyscale()
        .normalize()
        .contrast(0.8)
    .write('public/outputs/' + output_filename, callback); // save
    })
    .catch(err => {
      console.error(err);
    });

});

module.exports = router;
