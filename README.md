# Tesseract OCR

Small project using Tesseract.js (OCR) and Jimp.js (OCR preprocessing of images). It was written in WebStorm. 


## Prerequisites

This project uses Node.js and Express.js to run. It is necessary to install Node.js (only tested on Windows 10) and npm. 



## Installing

To install the project, call the following in the root directory:
```
npm install
```

To start the server, call the following in the root directory:
```
npm start
```

Then open the browser at:
```
http://localhost:3000/ocr/
```


## Authors

Linda Kolb

## License

This project is licensed under the MIT License



